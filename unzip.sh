myPath='unzip'
rm -rf $myPath

if [ ! -d "$myPath" ]; then
mkdir "$myPath"
fi

cd downfile &&

for zipfile in *.zip
do
folder=$(basename $zipfile .zip)
targetPath="../${myPath}/${folder}"
unzip $zipfile -d $targetPath
done