const logger = require('./utils/logger');
const shell = require('shelljs');

if (shell.exec(`npm run download`).code !== 0) {
    logger.error('下载出错！！！')
    shell.exit(1);
}
if (shell.exec(`npm run unzip`).code !== 0) {
    logger.error('解压出错！！！')
    shell.exit(1);
}
if (shell.exec(`npm run transfer`).code !== 0) {
    logger.error('转换数据出错！！！')
    shell.exit(1);
}
if (shell.exec(`npm run zip`).code !== 0) {
    logger.error('生成mext出错！！！')
    shell.exit(1);
}
