const logger = require('../utils/logger');
const _ = require('lodash');

function GenerateBlock(s2eFile, sources) {
    let blocks = [];
    let needFacePanel = false;
    let originBlocks = s2eFile.blockSpecs;
    let menus = s2eFile.menus || {};
    let values = s2eFile.values || {};
    let autoOpCodeList = {};
    originBlocks.forEach((item, blockIndex) => {
        logger.info('当前积木：', item[1])
        if (!item[1]) {
            return;
        }
        if (item.length <= 2) {
            if (blocks[blocks.length - 1])
                blocks[blocks.length - 1].gap = 36;
            return;
        }
        let { name, args } = transName(item[1]);
        let codeObj = _.isObject(item[item.length - 1]) ? item[item.length - 1] : {
            include: '',
            lib: '',
            declare: '',
            setup: '',
            code: '',
            _loop: '',
            loop: ''
        };
        let autoArgIndex = 0;
        let autoMenuIndexList = {};
        let argsNameList = [];
        if (autoOpCodeList[item[2]] != undefined) {
            if (autoOpCodeList[item[2]] == '') {
                autoOpCodeList[item[2]] = 1;
            } else {
                autoOpCodeList[item[2]]++;
            }
        } else {
            autoOpCodeList[item[2]] = '';
        }
        let block = {
            eid: 666,
            cid: 888,
            name: name,
            opcode: createOpCode(item, autoOpCodeList, blockIndex),
            blockType: getBlockType(item[0]),
            generatorCode: getBlockType(item[0])== 'hat' ? true : false,
            disableOnline: true,
            handler: {},
            args: args.map((menuName, index) => {

                if (autoMenuIndexList[menuName] != undefined) {
                    if (autoMenuIndexList[menuName] == '') {
                        autoMenuIndexList[menuName] = 1;
                    } else {
                        autoMenuIndexList[menuName]++;
                    }
                } else {
                    autoMenuIndexList[menuName] = '';
                }
                let arg;
                try {
                    if (menuName.includes('drawFace')) {
                        arg = {
                            type: 'facePanel',
                            name: String(menuName.includes('%') ? 'args' + autoArgIndex++ : menuName + autoMenuIndexList[menuName]),
                            val: '80dd5281-e401-4346-a1fd-64dce4a85461'
                        }
                        argsNameList.push(arg.name);
                    } else {
                        arg = {
                            type: menuName.includes('%n') ? 'number' : menuName.includes('%s') ? 'string' : 'fieldMenu',
                            name: menuName.includes('%') ? 'args' + autoArgIndex++ : menuName + autoMenuIndexList[menuName],
                            val: createArgsValue(menuName, item, index, values)
                        }
                        argsNameList.push(arg.name);
                        if (!menuName.includes('%')) {
                            if (menus[menuName] && _.isArray(menus[menuName])) {
                                arg['menus'] = menus[menuName].map(item => ({ text: item.toString(), value: values[item] != undefined ? values[item] : item }));
                            } else {

                                arg['menus'] = [{
                                    "text": arg.val.toString(),
                                    "value": arg.val
                                }];
                                logger.warn('缺失menu ', menuName, arg.val, arg['menus'])
                            }

                        }
                    }

                } catch (err) {
                    logger.error('error:', item, err);
                    arg = { type: '', name: '', val: '' }
                }


                return arg;
            }),
            codes: {
                arduinoc: {
                    include: '',
                    lib: createInclude(codeObj['inc'], sources),
                    declare: replaceArgHandle(codeObj['def']) != '' ? JSON.stringify(replaceArgHandle(codeObj['def']).split('--separator--')) : '',
                    setup: replaceArgHandle(codeObj['setup']),
                    code: replaceArgHandle(codeObj['work']) + (getBlockType(item[0]) == 'hat' ? '/*{$BRANCH}*/' : ''),
                    _loop: replaceArgHandle(codeObj['loop']),
                    loop: ''
                }
            },
            gap: 12,
            platform: ["mblockpc", "mblockweb"],
        };
        function replaceArgHandle(code) {
            if (code) {
                return code.replace(/\{\d+\}/g, (rep) => {
                    rep = rep.substring(1, rep.length - 1);
                    return `/*{${argsNameList[parseInt(rep)]}}*/`;
                })
            }
            return '';
        }
        blocks.push(block);
    })
    return { blocks, needFacePanel };
}
function createInclude(inc, sources) {
    // #include "LiquidCrystal_TR.h"
    if (inc) {
        let reg = /#\s*include\s*\"([A-z0-9_]+\.(h|cpp))\"/g;

        return inc.replace(reg, rep => {
            let tmpReg = new RegExp(reg);
            let result = tmpReg.exec(rep);
            if (result) {
                let tmpInc = sources.find(item => { return item.name.substr(4) == result[1] });
                if (tmpInc) {
                    return rep.replace('"', '"src/');
                } else {
                    return rep;
                }
            }
            else {
                return rep;
            }
        })
    }
    else {
        return '';
    }
}
function createOpCode(item, autoOpCodeList, blockIndex) {
    let op = '';
    if (item[2]) {
        op = item[2].replace(/[\s/,\.\+\-\*&%\(\)]/g, '_') + autoOpCodeList[item[2]];
        if (/\d/.test(op.charAt(0))) {
            op = 'm_' + op;
        }
    } else {
        op = 'mblock3_default_opcode_' + blockIndex;
    }
    return op;
}
function createArgsValue(menuName, item, index, values) {
    let value = '';
    if (menuName.includes('%')) {
        value = item[3 + index];
    } else {
        if (values[item[3 + index]] != undefined) {
            value = values[item[3 + index]];
        } else {
            value = item[3 + index];
        }

    }
    return value;
}
function getBlockType(type) {
    let newType = '';
    switch (type.toLowerCase()) {
        case 'h': newType = 'hat'; break;
        case 'w': newType = 'command'; break;
        case 'r': newType = 'number'; break;
        case 'b': newType = 'boolean'; break;
    }
    return newType;
}
function transName(name) {
    // 디지털 %n 핀에 %d.digital 보내기
    // adf @[String](string) @[Number](number)  @[布尔值](boolean) @[下拉框](fieldMenu) 
    // %n 数字 %d 数字列表  %s 字符串  %m 字符串列表
    // @[LED panel](facePanel)
    let args = [];
    name = name.replace(/\[/g,'(').replace(/\]/g,')');
    name = name.replace(/%[ns]|%[dm]\.[A-z0-9]+/g, item => {
        if (item.includes('%n')) {
            args.push(item);
            return '@[Number](number) '
        } else if (item.includes('%s')) {
            args.push(item);
            return '@[String](string) '
        } else if (item.includes('%d') || item.includes('%m')) {
            let menuName = item.trim().split('.').reverse()[0];
            args.push(menuName);
            if (menuName == 'drawFace') {
                needFacePanel = true;
                return '@[LED panel](facePanel) '
            }
            return '@[Dropdown](fieldMenu) '
        }
    })
    return { name, args };
}
module.exports = GenerateBlock;