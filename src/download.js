const fs = require('fs');
const http = require('http');
const fetch = require("node-fetch");
const path = require('path');
const shell = require('shelljs');
const logger = require('./utils/dllogger');
const emitter = require('./events/index');

const downloadPath = path.join(__dirname, '../downfile');

// 先清空文件夹
if (fs.existsSync(downloadPath)) {
    if (shell.exec(`rm -rf ${downloadPath}`).code !== 0) {
        logger.error('删除 downloadPath 失败')
        shell.exit(1);
    }
}
fs.mkdirSync(downloadPath);

http.get('http://www.mblock.cc/extensions/list.php', (res) => {
    const { statusCode } = res;
    logger.info('获取数据')

    let error;
    if (statusCode !== 200) {
        error = new Error('请求失败\n' +
            `状态码: ${statusCode}`);
    }
    if (error) {
        logger.error(error.message);
        // 消费响应数据来释放内存。
        res.resume();
        return;
    }

    res.setEncoding('utf8');
    let rawData = '';
    let currFileIndex = 0;
    let failCnt = 0;
    res.on('data', (chunk) => { rawData += chunk; });
    res.on('end', () => {
        try {
            const parsedData = JSON.parse(rawData);
            const FILELEN = parsedData.length;
            logger.info('len:', parsedData.length)
            function download(u, p, i) {
                return fetch(u, {
                    method: 'GET',
                    headers: { 'Content-Type': 'application/octet-stream' },
                }).then(res => res.buffer()).then(data => {
                    fs.writeFile(path.resolve(__dirname, downloadPath, p), data, "binary", function (err) {
                        logger.warn(err || i + ' ' + p);
                        currFileIndex++;
                        if (currFileIndex >= FILELEN) {
                            logger.info('download complete!');
                            logger.warn('失败个数：', failCnt);
                            emitter.emit('downloadComplete', {success: currFileIndex-failCnt, fail: failCnt});
                        }
                    });
                }).catch(err => {
                    currFileIndex++;
                    failCnt++;
                    logger.error('error:', parsedData[i].name + ' download error')
                    if (currFileIndex >= FILELEN) {
                        logger.info('download complete!');
                        logger.warn('失败个数：', failCnt);
                        emitter.emit('downloadComplete', {success: currFileIndex-failCnt, fail: failCnt});
                    }
                });
            }
            ////////======= 
            for (var i = 0; i < FILELEN; i++) {
                var url = 'http://www.mblock.cc/extensions/uploads/' + parsedData[i].download;
                download(url, url.split("/").reverse()[0], i)
            }

        } catch (e) {
            logger.error(e.message);
        }
    });
}).on('error', (e) => {
    logger.error(`出现错误: ${e.message}`);
});



