const log4js =  require('log4js');
const fs = require('fs');
const path = require('path');

const logpath = path.join('../../', 'logs')
if (!fs.existsSync(logpath)) {
    fs.mkdirSync(logpath);
}

let date = new Date();
let timestamp = `${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()}-${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}-`;
log4js.configure({
    appenders: {
    out: { type: 'stdout' },//设置是否在控制台打印日志
    info: { type: 'file', filename: `logs/${timestamp}info.log` },
    "just-warn": { type: 'file', filename: `logs/${timestamp}warn.log` },
    warn: { type: 'logLevelFilter', appender: 'just-warn', level: 'warn' },
    "just-errors": { type: 'file', filename: `logs/${timestamp}error.log` },
    error: { type: 'logLevelFilter', appender: 'just-errors', level: 'error' }
    },
    categories: {
        default: { appenders: [ 'out', 'info','error', 'warn' ], level: 'info' },//去掉'out'。控制台不打印日志
    }
});

module.exports = log4js.getLogger();