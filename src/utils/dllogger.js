const log4js =  require('log4js');
const fs = require('fs');
const path = require('path');

const logpath = path.join('../../', 'logs')
if (!fs.existsSync(logpath)) {
    fs.mkdirSync(logpath);
}

let date = new Date();
let timestamp = `${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()}-${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}-`;
log4js.configure({
    appenders: {
    out: { type: 'stdout' },//设置是否在控制台打印日志
    download: {type: 'file', filename: `logs/${timestamp}download.log`}, 
    },
    categories: {
        default: {appenders: ['download', 'out'], level: 'info'}
    }
});

module.exports = log4js.getLogger();