const fs = require('fs');
const path = require('path');
const shell = require('shelljs');
const logger = require('./logger');
let newfile = path.join(__dirname,'../../newfile');
class GenerateFile{
    constructor(){
        if(fs.existsSync(newfile)){
            if (shell.exec(`rm -rf ${newfile}`).code !== 0) {
                shell.exit(1);
            }
        }
        fs.mkdir(newfile);
    }
    mkdir(name){
        fs.mkdirSync(path.join(newfile, name));
        fs.mkdirSync(path.join(newfile, name, 'data.bak'));
    }
    writeFile(extName, fileName, data){
        fs.writeFile(path.join(newfile, extName, 'data.bak', fileName), data, (err) => {
            if (err) throw err;
            logger.info(`文件${extName}/${fileName}已被保存`);
        });
    }
}
module.exports = new GenerateFile();