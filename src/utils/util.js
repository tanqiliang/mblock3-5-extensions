const fs = require('fs');
const path = require('path');
const logger = require('./logger');
class FileSystem {
    get_s2e_file(url) {
        // console.log('get_s2e_file ',url)
        let fileList = fs.readdirSync(url);
        for (let i = 0; i < fileList.length; i++) {
            if(fileList[i]=='__MACOSX'){
                continue;
            }
            var filedir = path.join(url, fileList[i]);
            let stats = fs.statSync(filedir);
            let isFile = stats.isFile();//是文件
            var isDir = stats.isDirectory();//是文件夹
            // console.log('递归 ',isFile,isDir,fileList[i])
            if (isFile) {
                if (path.extname(fileList[i]) == '.s2e') {
                    return path.join(url, fileList[i]);
                }
            } else if (isDir) {
                let s2e = this.get_s2e_file(path.join(url, fileList[i]));
                if (s2e) {
                    return s2e;
                } else {
                    continue;
                }
            }
        }
        return null;
    }
    readFile(url, type) {
        let fileStr = fs.readFileSync(url, 'utf-8');
        let result = fileStr;
        try {
            if (type == 'json') {
                result = JSON.parse(fileStr);
            }
            return result;
        }
        catch (err) {
            try{
                eval(`result = ${fileStr}`);
                return result;
            }
            catch (err){
                logger.error('error:', url.split('unzip').reverse()[0] + '解析失败', err.message);
                return null;
            }
        }
    }
    readSrcFile(srcFiles, url) {
        try {
            let files = fs.readdirSync(url);
            files.forEach(item => {
                let stats = fs.statSync(path.join(url, item));
                let isFile = stats.isFile();//是文件
                if (isFile && ['.cpp', '.h'].includes(path.extname(item))) {
                    let code = this.readFile(path.join(url, item));
                    let cfile = {
                        name: 'src' + path.join(url, item).split('src')[1],
                        url: 'src' + path.join(url, item).split('src')[1],
                        fileData: code
                    }
                    srcFiles.push(cfile);
                } else if (stats.isDirectory()) {
                    this.readSrcFile(srcFiles, path.join(url, item));
                }
            })
        }catch(err){
            logger.warn(`${url}文件读取失败`);
        }
        
    }
}
module.exports = new FileSystem();