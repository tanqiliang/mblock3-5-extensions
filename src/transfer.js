const fs = require('fs');
const path = require('path');
const fileUtil = require('./utils/util');
const generateExt = require('./logic/generateExt');
const generateCategory = require('./logic/generateCategory');
const generateBlock = require('./logic/generateBlock');
const mfs = require('./utils/generatefile');
const logger = require('./utils/logger');
const generateFacePanel = require('./logic/generateFacepanel');

const unzipPath = path.join(__dirname, '../unzip');

let succCnt = 0;
let failCnt = 0;
let allCnt = 0;
let files = fs.readdirSync(unzipPath);//读取该文件夹
allCnt = files.length;
// logger.info(files)
files.forEach(dir => {
    let stats = fs.statSync(path.join(unzipPath, dir));
    if (stats.isDirectory()) {
        let s2e = fileUtil.get_s2e_file(path.join(unzipPath, dir));
        if (!s2e) {
            logger.error('no s2e file =>', dir);
            failCnt++;
        } else {
            logger.info(s2e)
            let s2efile = fileUtil.readFile(s2e, 'json');
            if (s2efile) {
                logger.info(dir,' => ',s2efile.extensionName)
                let m5ext = generateExt(s2efile, dir, s2e);
                let m5category = generateCategory(s2efile.extensionName);
                let {blocks, needFacePanel} = generateBlock(s2efile, m5ext.generator.arduinoc.sources);
                // logger.info(m5block[5]);
                let dirname = s2efile.extensionName.replace(/[\s/]/g, '_');
                mfs.mkdir(dirname);
                mfs.writeFile(dirname, 'ext.json', JSON.stringify(m5ext));
                mfs.writeFile(dirname, 'categroy.json', JSON.stringify(m5category));
                mfs.writeFile(dirname, 'block.json', JSON.stringify(blocks));
                mfs.writeFile(dirname, 'translate.json', JSON.stringify([]));
                if(needFacePanel){
                    mfs.writeFile(dirname, 'facepanel.json', JSON.stringify(generateFacePanel));
                }
                succCnt++;
            }else{
                failCnt++;
            }
        }
    }
})
logger.info(`---- 总共 ${allCnt} 个扩展，成功：${succCnt}个，失败${failCnt}个`)


