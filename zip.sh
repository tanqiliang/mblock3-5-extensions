#! /bin/bash

inputPath='./newfile'
outPath='mext'

rm -rf $outPath

if [ ! -d "$outPath" ]; then
mkdir "$outPath"
fi

cd $inputPath &&

for dir in `find . -maxdepth 1 -type d`
do
    if [ "$dir" != "$outPath" ] && [ "$dir" != "input" ] && [ "$dir" != "." ]
    then
    dirpath=`basename $dir`
    echo $dirpath
    cd $dir
    zip -r ../../$outPath/$dirpath.mext ./*
    cd -
    fi
done